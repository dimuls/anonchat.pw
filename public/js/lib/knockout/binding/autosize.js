$(function() {
  ko.bindingHandlers.autosize = {
    init: function(element, valueAccessor, allBindings) {
      var autosizeBinding = valueAccessor();
      if( ko.unwrap(autosizeBinding) ) {
        autosize(element);
      }
      var valueBinding = allBindings.get('value');
      if( ko.isObservable(valueBinding) ) {
        var resizeEvent = document.createEvent('Event');
        resizeEvent.initEvent('autosize:update', true, false);
        valueBinding.subscribe(function(value) {
          if( !value ) {
            setTimeout(function() {
              element.dispatchEvent(resizeEvent);
            }, 0);
          }
        })
      }
    },
    update: function(element, valueAccessor) {
      var autosizeBinding = valueAccessor();
        if( ko.unwrap(autosizeBinding) ) {
        autosize(element);
      } else {
        autosize.destroy(element);
      }
    }
  };
})
