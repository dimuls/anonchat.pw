$(function() {

  ko.bindingHandlers.ctrlEnter = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      var cb = ko.unwrap(valueAccessor());

      function keyDown(event) {
        if(event.keyCode == 13 && (event.ctrlKey || event.metaKey)) {
          event.preventDefault();
          cb(event);
          return false;
  	    } else {
          return true;
        }
      }

      $(element).on('keydown', keyDown);

      ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
          $(element).off('keydown', keyDown);
      });
    }
  };

  ko.bindingHandlers.chatScroll = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      var onBottom = true;
      var $e = $(element);
      function scrollToBottom() {
        $e.scrollTop($e.prop('scrollHeight'));
      }
      $e.scroll(_.debounce(function() {
        if( $e.scrollTop() + $e.height() > $e.prop('scrollHeight') - 20 ) {
          onBottom = true;
        } else {
          onBottom = false;
        }
      }, 100));
      var v = valueAccessor();
      if( ko.isObservable(v) ) {
        v.subscribe(function() {
          if( onBottom ) {
            _.delay(scrollToBottom);
          }
        })
      }
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
    }
};

  var app = $('#app-data').data();

  app.ChatRoom = function(cr) {

    cr.usersCount = ko.observable('');

    cr.connection = app.ChatRoom.Connection(cr);
    cr.messages = app.ChatRoom.Messages(cr);
    cr.editor = app.ChatRoom.Editor(cr);

    cr.connection.isConnected.subscribe(function(isConnected) {
      if( isConnected ) {
        cr.connection.send({ type: 'messages get' });
      }
    });

    cr.messageWrite = function(message) {
      cr.connection.send({ type: 'message write', data: message });
    };

    cr.messageWriteDone = function() {
      cr.editor.clear();
    }

    cr.messageWritten = function(message) {
      cr.messages.add(message);
    }

    cr.messagesSet = function(messageList) {
      cr.messages.set(messageList);
    }

    cr.usersCountSet = function(usersCount) {
      cr.usersCount(usersCount);
    }

    return cr;
  };





  app.ChatRoom.Messages = function(cr) {
    var ms = ko.observableArray();
    ms.max = 100;
    ms.count = ko.pureComputed(
      function() { return ms().length }
    );
    ms.set = function(m) {
      ms(_.chain(_.isArray(m) ? m : [m])
          .map(function(m) {
            return app.ChatRoom.Message(m, ms, cr); })
          .take(ms.max)
          .value());
    }
    ms.add = function(m) {
      ms(_.chain(_.isArray(m) ? m : [m])
          .map(function(m) {
            return app.ChatRoom.Message(m, ms, cr); })
          .concat(ms())
          .take(ms.max)
          .value());
    }
    ms.isAbsent = ko.pureComputed(function() {
      return ms.count() == 0;
    });
    return ms;
  };





  app.ChatRoom.Message = function(m, ms, cr) {
    m.header = '<b>#'+(m.id)+'</b> / ' + m.dt;
    return m;
  };





  app.ChatRoom.Editor = function(cr) {
    var e = {};
    e.text = ko.observable();
    e.clear = function() {
      e.text('');
    }
    e.write = function() {
      cr.messageWrite({ text: e.text() });
    }
    return e;
  };





  app.ChatRoom.Connection = function(cr) {
    var c = {};

    c.url = 'ws://'+window.location.host+'/'+cr.roomId+'/socket';
    c.ws = null;

    c.isConnected = ko.observable(false);
    c.isConnecting = ko.observable(false);

    c.connect = function() {

      if( c.isConnected() || c.isConnecting() ) {
        return;
      }

      c.isConnecting(true);

      c.ws = new WebSocket(c.url);

      c.ws.onopen = function() {
        c.isConnected(true);
        c.isConnecting(false);
        console.log('Websocket connection openned.');
      };

      c.ws.onclose = function() {
        c.isConnected(false);
        c.isConnecting(false);
        console.warn('Websocket connection closed. Reconnecting...');
        c.reconnect();
      };

      c.ws.onerror = function() {
        c.isConnected(false);
        c.isConnecting(false);
        console.warn('Websocket connection error. Reconnecting...');
        c.reconnect();
      };

      c.ws.onmessage = function(event) {
        var data;
        try {
          data = JSON.parse(event.data);
        } catch(e) {
          console.error('Websocket connection error: can\'t parse recieved data.');
        }
        c.handle(data);
      };
    }

    c.reconnect = function() {
      _.defer(c.connect);
    }

    c.handle = function(crEvent) {
      var handlerName = _.camelCase(crEvent.type);
      var handler = cr[handlerName];
      if( handler ) {
        handler(crEvent['data']);
      } else {
        console.error('Websocket chat room event "'+handlerName+'" handler not found.');
        console.debug(crEvent);
      }
    }

    c.send = function(crEvent) {
      if( c.isConnected() ) {
        c.ws.send(JSON.stringify(crEvent));
      }
    };

    (function reconnector() {
      c.reconnect();
      setTimeout(reconnector, 1000);
    })();

    return c;
  };

  app.chatRoom = app.ChatRoom({ roomId : app.roomId });

  ko.applyBindings(app.chatRoom);
  window.app = app;
});
