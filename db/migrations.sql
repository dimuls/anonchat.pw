
-- 1 up : initial schema

CREATE TABLE message (
  room_id       VARCHAR(37)     NOT NULL,
  message_id    INTEGER         NOT NULL,
  dt            TIMESTAMP       NOT NULL      DEFAULT now(),
  text          VARCHAR(5000)   NOT NULL,
  PRIMARY KEY (room_id, message_id)
);

-- 1 down

DROP table message;




-- 2 up : message_id computed default value

CREATE FUNCTION get_next_message_id() RETURNS trigger AS
$func$
BEGIN
  NEW.message_id :=
    COALESCE(MAX(message_id) + 1, 1)
      FROM message WHERE room_id = NEW.room_id;
  RETURN NEW;
END
$func$ LANGUAGE plpgsql;

CREATE TRIGGER next_message_id_computer
  BEFORE INSERT ON message
    FOR EACH ROW
      WHEN (NEW.message_id IS NULL)
    EXECUTE PROCEDURE get_next_message_id();

-- 2 down

DROP TRIGGER next_message_id_computer ON message;
DROP FUNCTION get_next_message_id();
