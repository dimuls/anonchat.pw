#!/bin/bash

if [ "$#" -ne 1 ]
then
  echo "Usage: ./db/init.sh DB_USER"
  exit 1
fi

DB_USER=$1
DB='anonchat'

# PostgreSQL 9.5
#sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
#sudo apt-get install wget ca-certificates
#wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
#sudo apt-get update
#sudo apt-get upgrade
#sudo apt-get install postgresql-9.5

# PostgreSQL 9.3 + memcached
# sudo apt-get install postgresql memcached

# PostgreSQL 9.3 + redis
sudo apt-get install postgresql redis-server

sudo useradd -r $DB_USER
sudo -u postgres createuser $DB_USER
sudo -u postgres createdb $DB
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB TO $DB_USER"
