package Anonchat::Controller::Main;
use Mojo::Base 'Mojolicious::Controller';

sub index {
  my $self = shift;

  $self->redirect_to('chat-room', room_id => 'main');
}

1;
