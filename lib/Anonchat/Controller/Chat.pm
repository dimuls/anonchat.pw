package Anonchat::Controller::Chat;
use Mojo::Base 'Mojolicious::Controller';

use Anonchat::Model::ChatRoom;


sub room {
  my ($self) = @_;
  my $room_id = $self->param('room_id');
  $self->render(room_id => $room_id);
}





sub socket {
  my ($self) = @_;

  my $user_id = $self->session->{'user_id'};
  my $room_id = $self->param('room_id');

  my $chat_room = Anonchat::Model::ChatRoom->new($self->app, $room_id);

  my $on_event = $chat_room->on(event => sub {
    my ($event) = @_;
    $self->send({ json => $event });
  });

  $self->on(json => sub {
    my ($self, $event) = @_;
    return unless $event;

    my $type = $event->{type};
    return unless $type;

    if( $type eq 'message write' ) {
      $chat_room->message_write($event->{data});
      $self->send({ json => {
          type => 'message write done'
      } });
    }

    elsif( $type eq 'messages get' ) {
      my $messages = $chat_room->messages_get;
      $self->send({ json => {
        type => 'messages set', data => $messages
      } });
    }
  });

  $self->on(finish => sub {
    my ($self) = @_;
    $chat_room->off(event => $on_event);
    $chat_room->user_leave($user_id);
  });

  $chat_room->user_enter($user_id);

  $self->inactivity_timeout(600);
}







1;
