package Anonchat::Model;

# use Mojo::Base -strict;
use strict;
use warnings;
use utf8;
use feature ':5.10';
use IO::Handle ();

use Mojo::Util qw/ slurp /;

my $REDIS_SCRIPTS = {};



sub new {
  my ($class, $app) = @_;

  my $self = bless {
    app => $app,
  }, $class;

  return $self;
}



# some usefull getters

sub app {
  my $self = shift;
  return $self->{app};
}

sub pg {
  my $self = shift;
  return $self->app->pg;
}

sub redis {
  my $self = shift;
  return $self->app->redis;
}

sub log_dump {
  my $self = shift;
  $self->app->log->debug($self->app->dumper(@_));
}



# Mojo::Pg usefull helpers

sub pg_query {
  my $self = shift;
  return $self->pg->db->query(@_);
}

sub pg_notify {
  my $self = shift;
  return $self->pg->pubsub->notify(@_);
}

sub pg_listen {
  my $self = shift;
  return $self->pg->pubsub->listen(@_);
}

sub pg_unlisten {
  my $self = shift;
  return $self->pg->pubsub->unlisten(@_);
}



# Redis usefull helpers

sub _set_redis_script_sha {
  my ($self, $script_name, $sha) = @_;
  $REDIS_SCRIPTS->{$script_name} = $sha;
}

sub _get_redis_script_sha {
  my ($self, $script_name) = @_;
  return $REDIS_SCRIPTS->{$script_name};
}

sub load_once_redis_script {
  my ($self, $name, $script) = @_;
  my $full_name = ref($self)."::$name";
  unless( $self->_get_redis_script_sha($full_name) ) {
    my $sha = $self->redis->script_load($script);
    $self->_set_redis_script_sha($full_name, $sha);
  }
}

sub eval_redis_script {
  my ($self, $name, @args) = @_;
  my $full_name = ref($self)."::$name";
  my $sha = $self->_get_redis_script_sha($full_name);
  return $self->redis->evalsha($sha, @args);
}

1;
