package Anonchat::Model::ChatRoom;

# use Mojo::Base -strict;
use strict;
use warnings;
use utf8;
use feature ':5.10';
use IO::Handle ();

use parent 'Anonchat::Model';

use Mojo::JSON qw/ to_json from_json /;
use Mojo::Util qw/ xml_escape /;



sub new {
  my ($class, $app, $room_id) = @_;
  my $self = $class->SUPER::new($app);

  $self->{room_id} = $room_id;

  $self->load_once_redis_script(user_enter => qq/
    local room = KEYS[1]
    local user_id = KEYS[2]
    redis.call('hincrby', room, user_id, 1)
    return redis.call('hlen', room)
  /);

  $self->load_once_redis_script(user_leave => qq/
    local room = KEYS[1]
    local user_id = KEYS[2]
    local count = tonumber(redis.call('hincrby', room, user_id, -1))
    if count < 1 then
      redis.call('hdel', room, user_id)
    end
    return redis.call('hlen', room)
  /);

  return $self;
}



# simple markup

sub _markup {
  my ($text) = @_;
  return unless $text;

  # restrict text length
  $text = substr($text, 0, 6000).' [...]'
    if length $text > 6000;

  # remove trailing spaces
  $text =~ s/^\s+|\s+$//g;
  return unless $text;

  # escape special symbols
  $text = xml_escape($text);

  # url
  # regexp source: https://gist.github.com/winzig/8894715
  state $url_re = qr/(?i)\b((?:https?:(?:\/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:[a-z]{2,13})\/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])|(?:(?<!@)[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:[a-z]{2,13})\b\/?(?!@)))/;

  my $buffer;
  while( $text =~ /$url_re/mg ) {
    my ($head, $url, $tail) = ($`, $1, $');
    my $href = $url;
    $href = "http://".$href
      unless $href =~ m|^https?://|;
    $buffer //= $head;
    $buffer .= "<a href=\"$href\">$url</a>";
    $buffer .= $tail;
  }
  $text = $buffer if $buffer;

  # quote
  $text =~ s|^&gt; *(.+?)$|<q>&gt; $1</q>|gm;

  # new lines
  $text =~ s|\n\n+|<br/><br/>|g;
  $text =~ s|\n|<br/>|g;

  # bold
  $text =~ s|\*\*\b([^*]+?)\b\*\*|<b>$1</b>|g;

  # italic
  $text =~ s|\*\b([^*]+?)\b\*|<i>$1</i>|g;

  # underlined
  $text =~ s|_\b([^*]+?)\b_|<u>$1</u>|g;

  # crossed
  $text =~ s|-\b([^*]+?)\b-|<s>$1</s>|g;

  return $text;
}



# usefull helper

sub room_id {
  my ($self) = @_;
  return $self->{room_id};
}



# main

sub user_enter {
  my ($self, $user_id) = @_;

  my $users_count = $self->eval_redis_script(
    user_enter => 2, $self->room_id, $user_id);

  $self->pg_notify($self->room_id => to_json({
    type => 'users count set', data => $users_count }));
}



sub user_leave {
  my ($self, $user_id) = @_;

  my $users_count = $self->eval_redis_script(
    user_leave => 2, $self->room_id, $user_id);

  $self->pg_notify($self->room_id => to_json({
    type => 'users count set', data => $users_count }));
}



sub message_write {
  my ($self, $message) = @_;

  $message->{text} = _markup($message->{text});
  return unless $message->{text};

  $message = $self->pg_query(q{
    INSERT INTO
      message(room_id, text) VALUES (?, ?)
    RETURNING
      message_id AS id,
      to_char(dt, 'dd-MM-yyyy / HH24:MI:SS') AS dt,
      text
  }, $self->room_id, $message->{text})->hash;

  $self->pg_notify($self->room_id => to_json({
    type => 'message written',
       data => $message
  }));
}



sub messages_get {
  my ($self) = @_;
  return $self->pg->db->query(q{
    SELECT
      message_id AS id,
      to_char(dt, 'dd-MM-yyyy / HH24:MI:SS') AS dt,
      text
    FROM message WHERE room_id = ?
    ORDER BY message_id DESC
    LIMIT 100;
  }, $self->room_id)->hashes;
}



# simple event engine

sub on {
  my ($self, $name, $cb) = @_;

  if( $name eq 'event' ) {
    return $self->pg_listen($self->room_id => sub {
      my ($pubsub, $event) = @_;
      $cb->(from_json($event));
    });
  }

  return $cb;
}

sub off {
  my ($self, $name, $cb) = @_;

  if( $name eq 'event' ) {
    $self->pg_unlisten($self->room_id => $cb);
  }
}

1;
