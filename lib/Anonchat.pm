package Anonchat;
use Mojo::Base 'Mojolicious';

use Mojo::Pg;
use Mojo::JSON qw/ from_json to_json /;
use Mojo::Util qw/ files slurp /;
use Redis::Fast;
use Math::Random::Secure qw/ irand /;

use Anonchat::Model::ChatRoom;



sub generate_user_id() {
  return join "", map { sprintf "%02x", $_ }
                  map { unpack "C", pack("I128", irand) } 0 .. 127;
}





sub startup {
  my $self = shift;

  $self->secrets([
    'rewghoygHyekdogPophitetobwedAsObjaidNassAc<ogOfud3OlgItciddUrvem',
  ]);

  my $config = $self->app->plugin('Config');



  # helpers

  $self->helper(pg => sub {
    state $pg = Mojo::Pg->new($config->{db_pg});
    return $pg;
  });

  $self->helper(redis => sub {
    state $redis = Redis::Fast->new(%{$config->{db_redis}});
    return $redis;
  });

  $self->helper(log_dump => sub {
    my $self = shift;
    $self->app->log->debug($self->dumper(@_));
  });



  # db migration

  $self->pg->migrations->from_file(
    $self->app->home->rel_file("db/migrations.sql")
      )->migrate;

  $self->redis->flushall;



  # routing

  my $r = $self->routes->under(sub {
    my $self = shift;
    $self->session->{'user_id'} = generate_user_id
      unless $self->session->{'user_id'};
  });

  $r->get('/')
    ->to('Main#index')
    ->name('main-index');

  $r->get('/:room_id', [room_id => qr/\w+/])
    ->to('Chat#room')
    ->name('chat-room');

  $r->websocket('/:room_id/socket', [room_id => qr/\w+/])
    ->to('Chat#socket')
    ->name('chat-socket');
}

1;
