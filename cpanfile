requires 'EV', '>= 4.0';
requires 'Mojolicious';
requires 'Mojo::Pg';
requires 'Redis::Fast';
requires 'Math::Random::Secure';
requires 'Text::Markdown';
